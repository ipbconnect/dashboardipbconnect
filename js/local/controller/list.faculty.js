	app.controller('listFacultyCtrl', function ($scope, $state, $filter, ngTableParams, FacultySvc, localStorageService) {

    $scope.tablefaculty = new ngTableParams({
        page: 1,
        count: 10
    }, {
        total: 0,
        getData: function ($defer, params) {
            var limit = params.count();
            var offset = params.page();
            FacultySvc.getFaculty(limit, offset, params.filter()).then(function (res) {

                if (res.data.total == 0){
                    $scope.showNoData = true;
                }
                else{
                    $scope.showNoData = false;
                } 

                params.total(res.data.total);
                $defer.resolve(res.data.results);
            })
        }
    })
    
    $scope.detail = function (id) {
        $state.go('app.singlefaculty', { id: id });
    }

});

