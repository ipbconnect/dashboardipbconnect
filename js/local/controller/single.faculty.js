app.controller('SingleFacultyCtrl', function ($scope, $stateParams, FacultySvc, $state, localStorageService, $http, alertService,  Flash) {

    var user = JSON.parse(localStorageService.get("user"));
    var id = $stateParams.id;
    $scope.form = {};  

    //initiate show button
    $scope.btnDelete = false;
    $scope.btnUpdate = false;
    $scope.btnSubmit = true;
    //get single
    if( id !== ""){

        $scope.btnDelete = true;
        $scope.btnUpdate = true;
        $scope.btnSubmit = false;

         FacultySvc.getFacultyById(id).then(function(res){
            $scope.form = res.data;
         });
    }    

    //save Faculty
    $scope.saveFaculty = function(form){
        if ($scope.form.name == "" || $scope.form.name == undefined){
           alertService.add('warning', 'field Nama Fakultas harap diisi');
           return;
        }
        $scope.form.created = new Date();
        $scope.form.createdBy = user._id;



       // service create events
        FacultySvc.createFaculty($scope.form).then(function (res) { //upload function returns a promise
             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'fakultas berhasil dibuat!');    
                $state.go("app.listfaculty");

            } else {
                Flash.create('danger', 'fakultas gagal dibuat!');
            }
        });


    };    

  $scope.updateFaculty = function(form){
        delete $scope.form.modifiedBy;
        $scope.form.modifiedBy = user._id;
       
        FacultySvc.updateFaculty($scope.form, id).then(function (res) { //upload function returns a promise
             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'update fakultas berhasil!');
                $state.go('app.listfaculty');
            } else {
                Flash.create('danger', 'update fakultas gagal!');
            }
        }, function (res) { //catch error

        }, function (evt) { 
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.progress = 'progress: ' + progressPercentage + '% '; // capture upload progress
        });

    }

    $scope.deleteFaculty = function(){

        FacultySvc.deleteFaculty(id).then(function(res){
            console.log(res);
            if(res.data.isSuccess){ //validate success
                Flash.create('success', 'delete fakultas berhasil!');
                $state.go('app.listfaculty');
            } else {
                Flash.create('danger', 'delete fakultas gagal!');
            }
        })

    } 
});