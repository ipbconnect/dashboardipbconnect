app.controller('SingleJobLocationCtrl', function ($scope, $stateParams, JobLocationSvc, $state, localStorageService, $http, alertService,Flash) {

    var user = JSON.parse(localStorageService.get("user"));
    var id = $stateParams.id;
    $scope.form = {};  

    //initiate show button
    $scope.btnDelete = false;
    $scope.btnUpdate = false;
    $scope.btnSubmit = true;
    //get single
    if( id !== ""){

        $scope.btnDelete = true;
        $scope.btnUpdate = true;
        $scope.btnSubmit = false;

         JobLocationSvc.getJobLocationById(id).then(function(res){
            $scope.form = res.data;
         });
    }    

    //save joblocation
    $scope.saveJobLocation = function(form){

        if ($scope.form.name == "" || $scope.form.name == undefined){
           alertService.add('warning', 'field Nama Provinsi harus diisi');
           return;
        }
        $scope.form.created = new Date();
        $scope.form.createdBy = user._id;



       // service create events
        JobLocationSvc.createJobLocation($scope.form).then(function (res) { //upload function returns a promise

             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'Lokasi kerja berhasil dibuat!');   
                $state.go("app.listjoblocation");

            } else {
                Flash.create('danger', 'Lokasi kerja gagal dibuat!');
            }
        });


    };    

  $scope.updateJobLocation = function(){
        delete $scope.form.modifiedBy;
        $scope.form.modifiedBy = user._id;
       
        JobLocationSvc.updateJobLocation($scope.form, id).then(function (res) { //upload function returns a promise

             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'update lokasi kerja berhasil!');
                $state.go('app.listjoblocation');
            } else {
                Flash.create('danger', 'update lokasi kerja gagal!');
            }
        });

    }

    $scope.deleteJobLocation = function(){

        JobLocationSvc.deleteJobLocation(id).then(function(res){
            if(res.data.isSucces){ //validate success
                Flash.create('success', 'delete lokasi gagal!');
                $state.go('app.listjoblocation');
            } else {
                Flash.create('danger', 'delete lokasi kerja gagal!');
            }
        })

    } 
});