app.factory('LowonganSvc', function ($http, config) {
    return {

        getLowongans: function (limit, offset, query) {

            var url = config.url + '/vacancies';
            var limit = '?limit=' + limit;
            var offset = '&page=' + offset;
            var filter1 = query.title == undefined ? "" : '&title=' + query.title;  
            var filter2 = query.company == undefined ? "" : '&company=' + query.company;
            var filter3 = query.salaryMin == undefined ? "" : '&salaryMin=' + query.salaryMin;                 
            var final = url + limit + offset +filter1 + filter2 + filter3 ;
            return $http.get(final);
        },
        createLowongan: function (data) {
            var url = config.url + '/vacancies';
            return $http.post(url, data);
        },
        getLowonganById: function(data){
            var id = data;
            var url = config.url + '/vacancies/' + id;
            return $http.get(url);
        },        
        deleteLowongan: function (id) {
            var url = config.url + '/vacancies/' + id;
            return $http.delete(url);
        },
        updateLowongan: function (data, id) {
            var url = config.url + '/vacancies/' + id;
            return $http.put(url, data);
        }    
    };
});