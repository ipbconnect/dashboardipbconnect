app.controller('SingleLowonganCtrl', function ($scope, $stateParams, LowonganSvc, $state, localStorageService, $http,mainMenuSvc, alertService, Flash) {

    var user = JSON.parse(localStorageService.get("user"));
    var id = $stateParams.id;
    $scope.data = {};
    $scope.form = {};
    //get location
    $scope.location = [];
    mainMenuSvc.getLocation().then(function(res){
      $scope.location = res.data;
      
    });    

    //initiate show button
    $scope.btnDelete = false;
    $scope.btnUpdate = false;
    $scope.btnSubmit = true;
    //get single
    if( id !== ""){

        $scope.btnDelete = true;
        $scope.btnUpdate = true;
        $scope.btnSubmit = false;

         LowonganSvc.getLowonganById(id).then(function(res){
            $scope.form = res.data;
            $scope.form.closeDate =  new Date(res.data.closeDate);
            $scope.data.province = res.data.jobLocationId;     
         });
    }    

    //datepicker
    $scope.open1 = function() {
    $scope.popup1.opened = true;
    };
    $scope.popup1 = {
    opened: false
    };

    //save lowongan
    $scope.saveLowongan = function(form){
        //validasi
        if ($scope.form.title == "" || $scope.form.title == undefined){
           Flash.create('warning', 'Judul lowongan harus diisi');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.subject == "" || $scope.form.subject == undefined){
           Flash.create('warning', 'subject lowongan harus diisi');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.email == "" || $scope.form.email == undefined){
           Flash.create('warning', 'Email harus diisi');
           window.scrollTo(0, 0);
           return;
        }          
        if ($scope.form.company == "" || $scope.form.company == undefined){
           Flash.create('warning', 'nama perusahaan harus diisi');
           window.scrollTo(0, 0);
           return;
        }                 
        if ($scope.form.companyProfile == "" || $scope.form.companyProfile == undefined){
           Flash.create('warning', 'Profil Perusahaan harus diisi');
           window.scrollTo(0, 0);
           return;
        } 
        if ($scope.form.address == "" || $scope.form.address == undefined){
           Flash.create('warning', 'Alamat Perusahaan harus diisi');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.data.province == "" || $scope.data.province == undefined){
           Flash.create('warning', 'Lokasi Perusahaan harus diisi');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.closeDate == "" || $scope.form.closeDate == undefined){
           Flash.create('warning', 'Tanggal Tutup lowongan harus diisi');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.salaryMin == "" || $scope.form.salaryMin == undefined){
           Flash.create('warning', 'Min Salary harus diisi');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.salaryMax == "" || $scope.form.salaryMax == undefined){
           Flash.create('warning', 'Max Salary harus diisi');
           window.scrollTo(0, 0);
           return;
        }        
        if ($scope.form.jobQualification == "" || $scope.form.jobQualification == undefined){
           Flash.create('warning', 'Kualifikasi Pekerjaan harus diisi');
           window.scrollTo(0, 0);
           return;
        }    
        if ($scope.form.jobDescription == "" || $scope.form.jobDescription == undefined){
           Flash.create('warning', 'Deskripsi Pekerjaan harus diisi');
           window.scrollTo(0, 0);
           return;
        }  
        if ($scope.form.file == "" || $scope.form.file == undefined){
           Flash.create('warning', 'Kelengkapan Dokumen harus diisi');
           window.scrollTo(0, 0);
           return;
        }                                  
        $scope.form.jobLocationId = $scope.data.province._id;
        $scope.form.createdBy = user._id;


       // service create events
        LowonganSvc.createLowongan($scope.form).then(function (res) { //upload function returns a promise

             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'Lowongan berhasil dibuat!');
                $state.go("app.listlowongan");;
            } else {
                Flash.create('danger', 'Lowongan gagal dibuat!');
            }
        });


    };    

  $scope.updateLowongan = function(){
        console.log($scope.form);
        delete $scope.form.jobLocationId;
        $scope.form.jobLocationId = $scope.data.province._id;


        LowonganSvc.updateLowongan($scope.form, id).then(function (res) { //upload function returns a promise

             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'update lowongan berhasil!');
                $state.go('app.listlowongan');
            } else {
                Flash.create('danger', 'update lowongan gagal!');
            }
        });

    }

    $scope.deleteLowongan = function(){

        LowonganSvc.deleteLowongan(id).then(function(res){
            if(res.data.isSuccess){ //validate success
                Flash.create('success', 'delete lowongan berhasil!');
                $state.go('app.listlowongan');
            } else {
                Flash.create('danger', 'delete lowongan gagal!');
            }
        })

    } });