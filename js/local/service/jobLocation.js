app.factory('JobLocationSvc', function ($http, config) {
    return {

        getJobLocation: function (limit, offset, query) {

            var url = config.url + '/joblocations';
            var limit = '?limit=' + limit;
            var offset = '&page=' + offset;
            var filter1 = query.name == undefined ? "" : '&name=' + query.name;              
            var final = url + limit + offset +filter1 ;
            return $http.get(final);
        },
        createJobLocation: function (data) {
            var url = config.url + '/joblocations';
            return $http.post(url, data);
        },
        getJobLocationById: function(data){
            var id = data;
            var url = config.url + '/joblocations/' + id;
            return $http.get(url);
        },        
        deleteJobLocation: function (id) {
            var url = config.url + '/joblocations/' + id;
            return $http.delete(url);
        },
        updateJobLocation: function (data, id) {
            var url = config.url + '/joblocations/' + id;
            return $http.put(url, data);
        }    
    };
});