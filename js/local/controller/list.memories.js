	app.controller('listMemoriesCtrl', function ($scope, $state, $filter, ngTableParams, MemoriesSvc, localStorageService,config, Lightbox, Flash) {

    $scope.tableMemories = new ngTableParams({
        page: 1,
        count: 10
    }, {
        total: 0,
        getData: function ($defer, params) {
            var limit = params.count();
            var offset = params.page() ;
            MemoriesSvc.getList(limit, offset,params.filter()).then(function (res) {
                if (res.data.total == 0){
                    $scope.showNoData = true;
                }
                else{
                    $scope.showNoData = false;
                } 
                params.total(res.data.total);
                $defer.resolve(res.data.results);
            })
        }
    }); 

    $scope.urlPhoto = function(x){
      if(x == undefined || x == ""){
        return "img/stock-event.jpg";
      }
      return config.url + "/uploads/memory/" + x;
    }
    $scope.deletePhoto = function(id){

        MemoriesSvc.delete(id).then(function(res){
            if(res.data.isSuccess){ //validate success
                Flash.create('success', 'Hapus memories berhasil!');
                $state.reload();
            } else {
                Flash.create('danger', 'Hapus memories gagal!');
            }
        })

    }
  $scope.openLightboxModal = function (picture) {
    Lightbox.openModal(picture, 0);
  };
  

});

