app.controller('SingleEventCtrl', function ($log,$scope, $stateParams, $http, $state, EventSvc, localStorageService,  $filter, Upload, NgMap, config,$timeout,alertService, Flash) {

    var user = JSON.parse(localStorageService.get("user"));
    var id = $stateParams.id;
    $scope.form = {};     //rebuild array createdBy;
    $scope.zoomed = 2;
    $scope.btnDelete = false;
    $scope.btnUpdate = false;
    $scope.btnSubmit = true;
    $scope.opt= {isFreeCharge : false, isSameDay :false};  
    $scope.today = new Date();
    $scope.showImageInput = true;
    $scope.form.price = undefined;
    $scope.setTrueShowImage = function(){
      $scope.showImageInput = true;
    }
    $scope.dontshow = false;

    $scope.urlPhoto = function(x){

      if( x != "")
      {
      return config.url + "/uploads/event/" + x;
      }
    }

    $scope.startDateOptions = {
      minDate: new Date()
    }
    
    if( id !== ""){
        $scope.showImageInput = false;
        if($scope.picture == ""){
          $scope.dontshow = true;
        }
        $scope.btnDelete = true;
        $scope.btnUpdate = true;
        $scope.btnSubmit = false;
         console.log($scope.dontshow,"dontshow" );
          console.log($scope.showImageInput, "showImageInput");

         EventSvc.getEventById(id).then(function(res){
          console.log(res.data);
            $scope.form = res.data;
            $scope.mytime = new Date(res.data.startTime);
            $scope.mytime2 = new Date(res.data.endTime);
            $scope.latlng = [res.data.latitude,res.data.longitude];
            $scope.form.startDate =  new Date(res.data.startDate);            
            $scope.form.endDate =  new Date(res.data.endDate);
            $scope.picture = res.data.picture;
            if(res.data.price == "0"){
                $scope.opt.isFreeCharge = true;
                $("#price").attr("disabled", "true");                
             }
         });  

    }   
  $scope.showPrice = function(){
    if($scope.opt.isFreeCharge){
      $("#price").attr("disabled", "true");
      $scope.form.price = 0;
    }else{
      $("#price").removeAttr("disabled");
    }
  };
  $scope.sameDays = function(){
    if($scope.opt.isSameDay){
      $("#sameDay").attr("disabled", "true");
      $scope.form.endDate = $scope.form.startDate;
      console.log($scope.form.endDate);
    }else{
      $("#sameDay").removeAttr("disabled");
    }
  };  

    $scope.saveEvents = function(){

        if ($scope.form.title == "" || $scope.form.title == undefined){
           Flash.create('warning', 'Judul Event harus diisi');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.place == "" || $scope.form.place == undefined){
           Flash.create('warning', 'Alamat Event harus diisi');
           window.scrollTo(0, 0);
           return;
        }         
        if ($scope.mytime == "" || $scope.mytime == undefined){
           Flash.create('warning', 'Waktu dimulai harus diisi');
           window.scrollTo(0, 0);
           return;
        }                 
        if ($scope.mytime2 == "" || $scope.mytime2 == undefined){
           Flash.create('warning', 'Waktu Selesai harus diisi');
           return;
        } 
        if ($scope.form.startDate == "" || $scope.form.startDate == undefined){
           Flash.create('warning', 'Tanggal Mulai harus diisi');
           window.scrollTo(0, 0);
           return;
        }         
        if ($scope.form.endDate == "" || $scope.form.endDate == undefined){
           Flash.create('warning', 'Tanggal Selesai harus diisi');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.description == "" || $scope.form.description == undefined){
           Flash.create('warning', 'Deskripsi Event harus diisi');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.contact == "" || $scope.form.contact == undefined){
           Flash.create('warning', 'Kontak harus diisi');
           window.scrollTo(0, 0);
           return;
        }        
        if ($scope.form.price == undefined ){
           Flash.create('warning', 'Harga harus diisi, pilih gratis jika gratis');
           window.scrollTo(0, 0);
           return;
        }
        $scope.form.startTime= $scope.mytime;   
        $scope.form.endTime = $scope.mytime2;
        $scope.form.picture = $scope.picture;
        $scope.form.latitude = $scope.lat;
        $scope.form.longitude = $scope.lang;
        $scope.form.createdBy = user._id;
        console.log($scope.form);
       // service create events
        EventSvc.createEvents($scope.form).then(function (res) { //upload function returns a promise

             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'Event berhasil dibuat!');
                $state.go("app.listevent");;
            } else {
                Flash.create('danger', 'Event gagal dibuat!');
            }
        });


    };

    //map
    $scope.latlng = [0,0];
    $scope.getpos = function(event){
       $scope.latlng = [event.latLng.lat(), event.latLng.lng()];
       $scope.lat = event.latLng.lat();
       $scope.lang = event.latLng.lng();
    };

    //map google autocomplete
  var vm = this;

  $scope.placeChanged = function() {
    vm.place = this.getPlace();
    vm.map.setCenter(vm.place.geometry.location);
    vm.map.setZoom(15);
    
  }
  NgMap.getMap().then(function(map) {
    vm.map = map;
  });
    //datepicker
    $scope.open1 = function() {
    $scope.popup1.opened = true;
    };
    $scope.popup1 = {
    opened: false
    };

    $scope.open2 = function() {
    $scope.popup2.opened = true;
    };
    $scope.popup2 = {
    opened: false
    };

  $scope.ismeridian = true;
  $scope.toggleMode = function() {
    $scope.ismeridian = ! $scope.ismeridian;
  };


  $scope.changed = function () {
    $log.log('Time changed to: ' + $scope.mytime);
  };


  $scope.updateEvents = function(form){
        delete $scope.form.modifiedBy;
        $scope.form.startTime= $scope.mytime;   
        $scope.form.endTime = $scope.mytime2;
        $scope.form.picture = $scope.picture;
        $scope.form.latitude = $scope.lat;
        $scope.form.longitude = $scope.lang;        
        $scope.form.modifiedBy = user._id;
        console.log($scope.form);
       
        EventSvc.updateEvents($scope.form, id).then(function (res) { 
             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'update event berhasil!');
                $state.go('app.listevent');
            } else {
                Flash.create('danger', 'update event gagal!');
            }
        });

    }

    $scope.deleteEvents = function(){

        EventSvc.deleteEvents(id).then(function(res){
            if(res.data.isSuccess){ //validate success
                Flash.create('success', 'delete event berhasil!');
                $state.go('app.listevent');
            } else {
                Flash.create('danger', 'delete event gagal!');
            }
        })

    } 


});