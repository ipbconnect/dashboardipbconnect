app.factory('FacultySvc', function ($http, config) {
    return {

        getFaculty: function (limit, offset, query) {

            var url = config.url + '/faculties';
            var limit = '?limit=' + limit;
            var offset = '&page=' + offset;
            var filter = query.name == undefined ? "" : '&name=' + query.name; 
            var final = url + limit + offset +filter ;
            return $http.get(final);
        },
        createFaculty: function (data) {
            var url = config.url + '/faculties';
            return $http.post(url, data);
        },
        getFacultyById: function(data){
            var id = data;
            var url = config.url + '/faculties/' + id;
            return $http.get(url);
        },        
        deleteFaculty: function (id) {
            var url = config.url + '/faculties/' + id;
            return $http.delete(url);
        },
        updateFaculty: function (data, id) {
            var url = config.url + '/faculties/' + id;
            return $http.put(url, data);
        }    
    };
});