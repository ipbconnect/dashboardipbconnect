app.factory('EventSvc', function ($http, config,Upload) {
    return {

        getEvents: function (limit, offset, query) {

            var url = config.url + '/events';
            var limit = '?limit=' + limit;
            var offset = '&page=' + offset;
            var filter1 = query.title == undefined ? "" : '&title=' + query.title;  
            var filter2 = query.place == undefined ? "" : '&place=' + query.place;              
            var final = url + limit + offset +filter1+filter2;
            return $http.get(final);
        },
        createEvents: function (data) {
            var url = config.url + '/events';
            return Upload.upload({url: url, data: data});

        },
        updateEvents: function (data, id) {
            var url = config.url + '/events/' + id;
            return Upload.upload({url:url, data:data, method: 'PUT'});
        },
        deleteEvents: function (id) {
            var url = config.url + '/events/' + id;
            return $http.delete(url);
        },
        getEventById: function (id) {
            var url = config.url + '/events/' + id;
            return $http.get(url);
        }      
    };
});