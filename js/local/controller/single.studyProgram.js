app.controller('SingleStudyProgramCtrl', function ($scope, $stateParams, StudyProgramSvc, $state, localStorageService, $http, mainMenuSvc, alertService,Flash) {

    var user = JSON.parse(localStorageService.get("user"));
    var id = $stateParams.id;
    $scope.form = {};  
    $scope.data = {};


    $scope.faculty = [];
    mainMenuSvc.getFaculty().then(function(res){
      $scope.faculty = res.data;
      
    }); 


    //initiate show button
    $scope.btnDelete = false;
    $scope.btnUpdate = false;
    $scope.btnSubmit = true;
    //get single
    if( id !== ""){

        $scope.btnDelete = true;
        $scope.btnUpdate = true;
        $scope.btnSubmit = false;

         StudyProgramSvc.getStudyProgramById(id).then(function(res){
            $scope.form = res.data;
            $scope.data.faculty= res.data.facultyId;    
         });
    }    

    //save joblocation
    $scope.saveStudyProgram = function(form){
        if ($scope.form.name == "" || $scope.form.name == undefined){
           Flash.create('warning', 'field Nama Program Studi harap diisi');
           return;
        }
        if ($scope.data.faculty == "" || $scope.data.faculty == undefined){
           Flash.create('warning', 'Fakultas harus dipilih');
           return;
        }



        
        
        $scope.form.facultyId = $scope.data.faculty._id;
        $scope.form.created = new Date();
        $scope.form.createdBy = user._id;



       // service create events
        StudyProgramSvc.createStudyProgram($scope.form).then(function (res) { //upload function returns a promise
             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'Program studi berhasil dibuat!');    
                $state.go("app.liststudyprogram");

            } else {
                Flash.create('danger', 'Program studi gagal dibuat!'); 
            }
        });


    };    

  $scope.updateStudyProgram = function(form){
        delete $scope.form.modifiedBy;
        $scope.form.modifiedBy = user._id;
        $scope.form.facultyId = $scope.data.faculty._id;
       
        StudyProgramSvc.updateStudyProgram($scope.form, id).then(function (res) { //upload function returns a promise
             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'Update Program studi berhasil !'); 
                $state.go('app.liststudyprogram');
            } else {
                Flash.create('danger', 'Update Program studi gagal!'); 
            }
        });

    }

    $scope.deleteStudyProgram = function(){

        StudyProgramSvc.deleteStudyProgram(id).then(function(res){
            if(res.data.isSuccess){ //validate success
                Flash.create('success', 'Delete Program studi berhasil!'); 
                $state.go('app.liststudyprogram');
            } else {
                Flash.create('danger', 'Delete Program studi gagal!'); 
            }
        })

    } 
});