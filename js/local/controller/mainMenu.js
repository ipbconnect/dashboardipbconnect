﻿app.controller('MainMenuCtrl', function ($scope, mainMenuSvc, $filter,  localStorageService, $state, AnggotaSvc, StudyProgramSvc,FacultySvc ) {
  
    AnggotaSvc.getUserNotVerified('', '', {}).then(function (res) {
        $scope.totalRequest = res.data.total;
    })
        
    AnggotaSvc.getTotalAnggota('', '', {}).then(function (res) {
        $scope.totalAnggota = res.data.total;
    })
    StudyProgramSvc.getStudyProgram('', '', {}).then(function (res) {
        $scope.totalStudyProgram = res.data.total;
    })        
    FacultySvc.getFaculty('', '', {}).then(function (res) {
        $scope.totalFaculty = res.data.total;
        console.log(res.data.results)
    })      
    AnggotaSvc.countAngkatan().then(function (res) {
        var count = [];
        var batch = [];
        var results = res.data.results;
        results.sort(function(a, b){
            return a._id.batch - b._id.batch;
        });
        console.log(results);
        for(i=0;i < results.length; i++){
           count[i] = results[i].count;
           batch[i] = results[i]._id.batch;  
        } 
        var ctx = document.getElementById("myChart");

        var randomColorGenerator = function () { 
            return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
        };
        var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: batch,
            datasets: [{
                label: 'Jumlah Anggota Per Angkatan',
 
                borderWidth: 1,
                backgroundColor: 'rgba(35,144,255,0.8)',
                data: count                          
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
        }); 

    }) 

      
});
