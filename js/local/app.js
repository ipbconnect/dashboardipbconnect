var app = angular.module('App', ['ngRadialGauge',
    'angular-svg-round-progress',
    'chart.js', 'ui.router',
    'ngTable',
    'LocalStorageModule',
    "kendo.directives",
    'ui.select',
    'angular-loading-bar',
    'ngSanitize', 'ui.bootstrap','ngFileUpload','ngMap', 'ngAnimate', 'ui.select','ngFlash','bootstrapLightbox'
    ]);

app.controller('MainCtrl', function ($scope, localStorageService) {

});    

app.constant("config", {
    
    url: "http://api.stage.ipb-connect.com"
    

});

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider

    .state('app', {
        url: '/',
        abstract:true,
        templateUrl: 'tpl/app.html',
        controller: 'AppCtrl'
        
    })

    .state('app.mainmenu', {
        url: '',
        templateUrl: 'tpl/mainMenu.html',
        controller: 'MainMenuCtrl'
        
    })

    //list
    .state('app.listlowongan', {
        url: 'listLowongan',
        templateUrl: 'tpl/list.lowongan.html',
        controller: 'listLowonganCtrl'
    })
    .state('app.listadmin', {
        url: 'listAdmin',
        templateUrl: 'tpl/list.admin.html',
        controller: 'listAnggotaCtrl'
    })
    .state('app.listevent', {
        url: 'listEvent',
        templateUrl: 'tpl/list.event.html',
        controller: 'listEventCtrl'
    })
    .state('app.listanggota', {
        url: 'listAnggota',
        templateUrl: 'tpl/list.anggota.html',
        controller: 'listAnggotaCtrl'
    })
    .state('app.listjoblocation', {
        url: 'listjoblocation',
        templateUrl: 'tpl/list.jobLocation.html',
        controller: 'listJobLocationCtrl'
    })   
    .state('app.liststudyprogram', {
        url: 'liststudyprogram',
        templateUrl: 'tpl/list.studyProgram.html',
        controller: 'listStudyProgramCtrl'
    })
    .state('app.listrequestanggota', {
        url: 'listrequestanggota',
        templateUrl: 'tpl/list.requestAnggota.html',
        controller: 'listRequestAnggotaCtrl'
    })   
    .state('app.listfaculty', {
        url: 'listfaculty',
        templateUrl: 'tpl/list.faculty.html',
        controller: 'listFacultyCtrl'
    })      
    .state('app.listmemories', {
        url: 'listmemories',
        templateUrl: 'tpl/list.memories.html',
        controller: 'listMemoriesCtrl'
    })                      
    //single
    .state('app.singleevent', {
        url: 'singleevent/:id',
        templateUrl: 'tpl/single.event.html',
        controller: 'SingleEventCtrl'
    })
    .state('app.singlelowongan', {
        url: 'singlelowongan/:id',
        templateUrl: 'tpl/single.lowongan.html',
        controller: 'SingleLowonganCtrl'
    })
    .state('app.singleanggota', {
        url: 'singleanggota/:id',
        templateUrl: 'tpl/single.anggota.html',
        controller: 'SingleAnggotaCtrl'
    })
    .state('app.singlejoblocation', {
        url: 'singlejoblocation/:id',
        templateUrl: 'tpl/single.jobLocation.html',
        controller: 'SingleJobLocationCtrl'
    })
    .state('app.singlestudyprogram', {
        url: 'singlestudyprogram/:id',
        templateUrl: 'tpl/single.studyProgram.html',
        controller: 'SingleStudyProgramCtrl'
    })     
    .state('app.singlefaculty', {
        url: 'singlefaculty/:id',
        templateUrl: 'tpl/single.faculty.html',
        controller: 'SingleFacultyCtrl'
    })            

    
    .state('app.changepassword', {
        url: 'changepassword/:id',
        templateUrl: 'tpl/changepassword.html',
        controller: 'SingleAnggotaCtrl'
    })
    .state('login', {
        url: '/login',
        templateUrl: 'tpl/login.html',
        controller: 'LoginCtrl'
    })

    $urlRouterProvider.otherwise('/');

     $httpProvider.interceptors.push(function ($q, $location, localStorageService,Flash) {
        return {
            'request': function (config) {
                config.headers = config.headers || {};
                var token = localStorageService.get("token");
                if (token) {
                    config.headers.Authorization = token;
                }
                return config;
            },
            'responseError': function (response) {
                if (response.status === 401 || response.status === 403) {
                    $location.path('/login');
                    Flash.create('danger','Anda tidak memiliki hak akses!');
                } else if (response.status === 500) {
                    Flash.create('danger','Kesalahan jaringan, mohon periksa jaringan internet anda!');
                }else{

                    Flash.create('danger','Mohon maaf telah terjadi kesalahan sistem');
                }
                return $q.reject(response);
            }
        };
    });

});


app.run(function ($rootScope, $state, $stateParams, localStorageService,Flash) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if (toState.name !== 'login' && localStorageService.get('user') == null && localStorageService.get('token') == null) {
            event.preventDefault();
            $state.go('login');
        }        

        $rootScope.root = {
            state: toState.name
        };
    });
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

});


app.config(function (localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('api');
    localStorageServiceProvider.setStorageType('localStorage'); // localStorage or sessionStorage
});

