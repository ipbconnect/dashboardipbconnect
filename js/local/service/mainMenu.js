﻿app.factory('mainMenuSvc', function ($http, config, localStorageService) {
    return {

        getStudyProgram: function () {
            var url = config.url + '/studyprograms/getlist';
            return $http.get(url);
        },
        getLocation: function () {
            var url = config.url + '/joblocations/getlist';
            return $http.get(url);
        },
        getFaculty: function () {
            var url = config.url + '/faculties/getlist';
            return $http.get(url);
        }
    };
});