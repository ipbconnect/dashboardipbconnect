app.controller('SingleAnggotaCtrl', function ($scope, $stateParams, $http, $state, AnggotaSvc, localStorageService,  $filter,  config, $timeout, mainMenuSvc, alertService,Flash) {

    var user = JSON.parse(localStorageService.get("user"));
    var id = $stateParams.id;

    $scope.form = { fullName : "",
                    gender : "",
                    email: "",
                    password : "",
                    dateOfBirth: "",
                    userType: "",
                    nim: "",
                    batch: "",
                    studyProgram : null,
                    isVerified : false
                  };

    //get program study                  
    $scope.programStudy = [];
    mainMenuSvc.getStudyProgram().then(function(res){
      $scope.programStudy = res.data;  
    });

    //initiate show button
    $scope.btnDelete = false;
    $scope.btnUpdate = false;
    $scope.btnSubmit = true;

    //get single
    if( id !== ""){

        $scope.btnDelete = true;
        $scope.btnUpdate = true;
        $scope.btnSubmit = false;

         AnggotaSvc.getUserById(id).then(function(res){

            $scope.form = res.data;
            $scope.form.studyProgram = res.data.studyProgramId;
            $scope.form.dateOfBirth =  new Date(res.data.dateOfBirth);  
         });
    }         


  //datepicker
  $scope.open1 = function() {
  $scope.popup1.opened = true;
  };
  $scope.popup1 = {
  opened: false
  };
  //input type password
  $scope.inputType = 'password';
  
 
     //save
    $scope.saveAnggota = function(form){
        if ($scope.form.fullName == "" || $scope.form.fullName == undefined){
           Flash.create('warning', 'Nama Lengkap harus diisi!');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.email == "" || $scope.form.email == undefined){
           Flash.create('warning', 'Email harus diisi!');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.password == "" || $scope.form.password == undefined){
           Flash.create('warning', 'Password harus diisi!');
           window.scrollTo(0, 0);
           return;
        }          
        if ($scope.form.gender == "" || $scope.form.gender == undefined){
           Flash.create('warning', 'Jenis Kelamin harus diisi!');
           window.scrollTo(0, 0);
           return;
        }                 
        if ($scope.form.dateOfBirth == "" || $scope.form.dateOfBirth == undefined){
           Flash.create('warning', 'Tanggal Lahir harus diisi!');
           window.scrollTo(0, 0);
           return;
        } 
        if ($scope.form.nim == "" || $scope.form.nim == undefined){
           Flash.create('warning', 'NIM harus diisi!');
           window.scrollTo(0, 0);
           return;
        }         
        if ($scope.form.studyProgram == "" || $scope.form.studyProgram == undefined){
           Flash.create('warning', 'Program Studi harus diisi!');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.batch == "" || $scope.form.batch == undefined){
           Flash.create('warning', 'Angkatan harus diisi!');
           window.scrollTo(0, 0);
           return;
        }
        if ($scope.form.userType == "" || $scope.form.userType == undefined){
           Flash.create('warning', 'Jenis Anggota harus diisi!');
           window.scrollTo(0, 0);
           return;
        }        
        $scope.form.isVerified = 1;
        $scope.form.studyProgramId = $scope.form.studyProgram._id;
        AnggotaSvc.createAnggota($scope.form).then(function (res) { 
              console.log(res.data);
             if(res.data.isSuccess){ 
                Flash.create('success', 'Tambah Anggota berhasil!');
                $state.go("app.listanggota");                
            } else {
                Flash.create('danger', 'Gagal tambah anggota!');
            }
        });


    };
  $scope.updateAnggota = function(){

        $scope.form.newPass = $scope.form.password;
        $scope.form.studyProgramId = $scope.form.studyProgram._id;        
        AnggotaSvc.updateAnggota($scope.form, id).then(function (res) { 
             if(res.data.isSuccess){ 
                Flash.create('success', 'Update Anggota berhasil !');
                $state.go('app.listanggota');
            } else {
                Flash.create('danger', 'Gagal update anggota!');
            }
        });
    }

    $scope.deleteAnggota = function(){

        AnggotaSvc.deleteAnggota(id).then(function(res){
            if(res.data.isSuccess){ //validate success
                Flash.create('success', 'Hapus anggota berhasil!');
                $state.go('app.listanggota');
            } else {
                Flash.create('danger', 'Gagal hapus anggota!');
            }
        })
    }

    $scope.setAdmin = function(){

            AnggotaSvc.setAdmin(id).then(function (res) { //upload function returns a promise
             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'Set hak admin berhasil!');
                $state.reload();
            } else {
                Flash.create('danger', 'Gagal Set hak admin!');
            }
        });   
    }
    $scope.unSetAdmin = function(){

            AnggotaSvc.unSetAdmin(id).then(function (res) { //upload function returns a promise
             if(res.data.isSuccess){ //validate success
                Flash.create('success', 'cabut hak admin berhasil!');
                $state.reload();            
            } else {
                Flash.create('danger', 'Gagal cabut hak admin!');
            }
        });   
    }
    $scope.toChangePassword = function(){
      $state.go('app.changepassword', { id: id });
    }
    $scope.changepassword = function(){
            AnggotaSvc.changepassword($scope.form,id).then(function (res) { 
             if(res.data.isSuccess){ 
                Flash.create('success', 'Ganti Password Berhasil');
                $state.go('app.singleanggota', { id: id });           
            } else {
                Flash.create('danger', 'Ganti Password Gagal!');
            }
           
        });


    }

});