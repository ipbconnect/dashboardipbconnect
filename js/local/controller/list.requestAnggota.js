	app.controller('listRequestAnggotaCtrl', function ($scope, $state, $filter, ngTableParams, AnggotaSvc, localStorageService, Flash) {

    $scope.tableRequestAnggota = new ngTableParams({
        page: 1,
        count: 10
    }, {
        total: 0,
        getData: function ($defer, params) {
            var limit = params.count();
            var offset = params.page();
            AnggotaSvc.getUserNotVerified(limit, offset, params.filter()).then(function (res) {
                if (res.data.total == 0){
                    $scope.showNoData = true;
                }
                else{
                    $scope.showNoData = false;
                }    
                params.total(res.data.total);
                $scope.size = res.data.Count;
                $defer.resolve(res.data.results);
                })
        }
    })

    
    
    $scope.approveRequest = function(id){
            AnggotaSvc.verified(id).then(function (res) { 
             if(res.data.isSuccess){ 
                Flash.create('success', 'Anggota Baru Diterima ! ');
                $state.reload();
            } else {
                Flash.create('danger', 'Gagal terima anggota baru ! ');
            }
        })   
    }
    $scope.refuseRequest = function(id){
        AnggotaSvc.deleteAnggota(id).then(function(res){
            if(res.data.isSuccess){ 
                Flash.create('success','Anggota ditolak!');
                $state.reload();
            } else {
                Flash.create('danger', 'Gagal tolak anggota ! ');
            }
        })
    }
});

