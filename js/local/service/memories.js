app.factory('MemoriesSvc', function ($http, config) {
    return {

        delete: function (id) {
            var url = config.url + '/memories/' + id;
            return $http.delete(url);
        },
        getList: function (limit, page, query) {
            var filter = query.caption == undefined ? "" : '&title=' + query.title;  
            var url = config.url + '/memories?limit=' + limit + '&page=' + page;
            return $http.get(url);
        },
    };
});