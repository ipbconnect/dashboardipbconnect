app.factory('AnggotaSvc', function ($http, config, Upload) {
    return {

        getAnggotas: function (limit, offset, query) {

            var filter1 = '&isVerified=' + 1 + '&isAdmin=' + 0;
            var url = config.url + '/users'  ;
            var limit = '?limit=' + limit;
            var offset = '&page=' + offset;
            var filter2 = query.fullName == undefined ? "" : '&fullName=' + query.fullName;  
            var filter3 = query.nim == undefined ? "" : '&nim=' + query.nim;   

            var filter4 = query.batch == undefined ? "" : '&batch=' + query.batch; 
            var filter5 = query.email == undefined ? "" : '&email=' + query.email;
            var filter6 = query.jurusan == undefined ? "" : '&jurusan=' + query.jurusan;                     
            var final = url + limit +offset + filter1 + filter2 + filter3 + filter4 + filter5 + filter6;

            console.log(final);
            return $http.get(final);
        },
        getTotalAnggota: function (limit, offset, query) {

            var filter1 = '&isVerified=' + 1 ;
            var url = config.url + '/users'  ;
            var limit = '?limit=' + limit;
            var offset = '&page=' + offset;
            var filter2 = query.fullName == undefined ? "" : '&fullName=' + query.fullName;  
            var filter3 = query.nim == undefined ? "" : '&nim=' + query.nim;   

            var filter4 = query.batch == undefined ? "" : '&batch=' + query.batch; 
            var filter5 = query.email == undefined ? "" : '&email=' + query.email;
            var filter6 = query.jurusan == undefined ? "" : '&jurusan=' + query.jurusan;                     
            var final = url + limit +offset + filter1 + filter2 + filter3 + filter4 + filter5 + filter6;

            console.log(final);
            return $http.get(final);
        },        
        getUserAdmin: function (limit, offset, query) {
            var filter1 = '&isVerified=1' + '&isAdmin=1' ;
            var url = config.url + '/users'  ;
            var limit = '?limit=' + limit;
            var offset = '&offset=' + offset;
          var filter2 = query.fullName == undefined ? "" : '&fullName=' + query.fullName;  
            var filter3 = query.nim == undefined ? "" : '&nim=' + query.nim;    
            var filter4 = query.batch == undefined ? "" : '&batch=' + query.batch; 
            var filter5 = query.email == undefined ? "" : '&email=' + query.email;
            var filter6 = query.jurusan == undefined ? "" : '&jurusan=' + query.jurusan;                     
            var final = url + limit +offset + filter1 + filter2 + filter3 + filter4 +filter5 + filter6;
            return $http.get(final);
        },
        getUserNotVerified: function (limit, offset, query) {
            var filter1 = '&isVerified=' + 0 ;
            var url = config.url + '/users'  ;
            var limit = '?limit=' + limit;
            var offset = '&page=' + offset;
            var filter2 = query.fullName == undefined ? "" : '&fullName=' + query.fullName;  
            var filter3 = query.nim == undefined ? "" : '&nim=' + query.nim;    
            var filter4 = query.batch == undefined ? "" : '&batch=' + query.batch; 
            var filter5 = query.email == undefined ? "" : '&email=' + query.email;
            var filter6 = query.jurusan == undefined ? "" : '&jurusan=' + query.jurusan;                     
            var final = url + limit +offset + filter1 + filter2 + filter3 + filter4 +filter5 + filter6;
            return $http.get(final);
        },                
        login: function(data){
            var url = config.url + "/users/login";
            return $http.post(url, data);
        },
        loginAdmin: function(data){
            var url = config.url + "/users/login";
            return $http.post(url, data);
        },        
        getUserById: function(data){
            var id = data;
            var url = config.url + '/users/' + id;
            return $http.get(url);
        },
        createAnggota: function (data) {
            var url = config.url + '/users';
            return $http.post(url, data);
        },        
        updateAnggota: function (data, id) {
            var url = config.url + '/users/academic/' + id;
            return $http.put(url, data);
        },        
        deleteAnggota: function (id) {
            var url = config.url + '/users/' + id;
            return $http.delete(url);
        },
        verified: function (id) {
            var url = config.url + '/users/verified/' + id;
            return $http.post(url);
        },        

        setAdmin: function(id){
            var url = config.url + '/users/setadmin/' + id;
            return $http.post(url);
        },
        unSetAdmin: function(id){
            var url = config.url + '/users/unsetadmin/' + id;
            return $http.post(url);
        },
        changepassword: function(data,id){
            var url = config.url + '/users/change-password/' + id + '/admin';
            return $http.put(url,data);
        },
        countAngkatan: function () {

            var url = config.url + '/users/count-batch'  ;
                

            return $http.get(url);
        },                

    };
});