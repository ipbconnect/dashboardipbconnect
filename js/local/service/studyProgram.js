app.factory('StudyProgramSvc', function ($http, config) {
    return {

        getStudyProgram: function (limit, offset, query) {

            var url = config.url + '/studyprograms';
            var limit = '?limit=' + limit;
            var offset = '&page=' + offset;
            var filter = query.name == undefined ? "" : '&name=' + query.name;  
            var final = url + limit + offset +filter;
            return $http.get(final);
        },
        createStudyProgram: function (data) {
            var url = config.url + '/studyprograms';
            return $http.post(url, data);
        },
        getStudyProgramById: function(data){
            var id = data;
            var url = config.url + '/studyprograms/' + id;
            return $http.get(url);
        },        
        deleteStudyProgram: function (id) {
            var url = config.url + '/studyprograms/' + id;
            return $http.delete(url);
        },
        updateStudyProgram: function (data, id) {
            var url = config.url + '/studyprograms/' + id;
            return $http.put(url, data);
        }    
    };
});